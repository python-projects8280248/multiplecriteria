# Multicriteria assessment

## Description

This project implements a multicriteria assessment using the Pareto method for analyzing and ranking solutions. The project includes a graphical user interface (GUI) that allows you to visualize data points, as well as perform performance analysis and ranking.

## Project structure

- `main.py `: The main file containing the code for the graphical user interface and interaction with modules.
- `pareto.py `: A module that implements functions for the Pareto method and the ranking of data points.
- `figures.py `: A module containing functions for generating and displaying graphs.

## Environment requirements

- Python 3.x
- Libraries: NumPy, Matplotlib, Tkinter

## Usage examples

- Graphical display of data points.
- Analysis of ineffective solutions and the construction of appropriate graphs.
- Ranking of points based on the efficiency index.

## Notes

- The project is designed to illustrate and understand the Pareto method in multicriteria assessment.
- These points are randomly generated to demonstrate how the method works.
