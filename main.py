import Figures
import Pareto_method
from tkinter import *


n = 12 #Радиус окружности/номер варианта
N = 20 #Количество точек

xPoints, yPoints = Figures.rejection_sampling(n, N)
numbers, b, F, K, r1, r2, r3 = Pareto_method.ranking(xPoints, yPoints, N)


def paint():
    Figures.show(xPoints, yPoints, n, N)


def pareto():
    Pareto_method.inefficient_solutions(xPoints, yPoints, N)


def ranking():
    Pareto_method.ranking_table(xPoints, yPoints, numbers, b, F, K, r1, r2, r3)


def paint_inefficient_solutions():
    Figures.paint_cones(xPoints, yPoints, N, 'inefficient')


def paint_dominance_cones():
    Figures.paint_cones(xPoints, yPoints, N, 'dominance')


def paint_ranking():
    Figures.paint_ranking_graphic(xPoints, yPoints, K, n, N)


window = Tk()
window.title("Добро пожаловать в приложение Мультикритериальная оценка")
window.geometry('400x500')


btnGraphics = Button(window, text="Графики", command=paint)
btnGraphics.grid(column=1, row=1)
btnInSolutions = Button(window, text="Искл. заведомо неэфф. решений\nГрафик", command=paint_inefficient_solutions)
btnInSolutions.grid(column=1, row=2)
btnRanks = Button(window, text="Искл. заведомо неэфф. решений", command=pareto)
btnRanks.grid(column=1, row=3)
btnRanks = Button(window, text="Построение конусов доминирования", command=paint_dominance_cones)
btnRanks.grid(column=1, row=4)
btnInSolutions = Button(window, text="Ранжирование на основе индекса эффективности", command=ranking)
btnInSolutions.grid(column=1, row=5)
btnInSolutions = Button(window, text="Ранжирование на основе индекса эффективности\nГрафик", command=paint_ranking)
btnInSolutions.grid(column=1, row=6)

window.mainloop()

