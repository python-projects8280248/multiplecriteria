import numpy as np
import matplotlib.pyplot as plt


def inefficient_solutions(xPoints, yPoints, N):

    fig, ax = plt.subplots()

    fig.patch.set_visible(False)
    ax.axis('off')
    ax.axis('tight')

    numbers = []
    for i in range(0, N):
        numbers.append(i+1)

    checkboxes = [''] * N
    crosses = [''] * N
    ignore = []
    for i in range(0, N):
        for j in range(0, N):
            if xPoints[i] >= xPoints[j] and yPoints[i] >= yPoints[j] and i != j:
                crosses[j] = 'x'
                ignore.append(j)
        if i not in ignore:
            checkboxes[i] = '+'

    points = [numbers, xPoints, yPoints, checkboxes, crosses]
    columns = ['i', 'F1', 'F2', 'checkbox', 'cross']

    ax.table(cellText=np.array(points).T, colLabels=columns, loc='center')

    fig.tight_layout()

    plt.show()


def ranking(xPoints, yPoints, N):
    numbers = []
    b = []
    F = []
    r1 = []
    r2 = []
    r3 = []
    K = []
    for i in range(0, N):
        b.append(0)
        F.append(0)
        numbers.append(i + 1)
    for i in range(0, N):
        for j in range(0, N):
            if xPoints[i] <= xPoints[j] and yPoints[i] <= yPoints[j] and i != j:
                b[i] += 1
    for i in range(0, N):
        F[i] = round(1 / (1 + (b[i] / (N - 1))), 2)
        r1.append(round(abs(1 - F[i]), 2))
        r2.append(round(abs(0.85 - F[i]), 2))
        r3.append(round(abs(0.75 - F[i]), 2))
        if r1[i] < r2[i] and r1[i] < r3[i]:
            K.append(1)
        if r2[i] < r1[i] and r2[i] < r3[i]:
            K.append(2)
        if r3[i] < r2[i] and r3[i] < r1[i]:
            K.append(3)
    return numbers, b, F, K, r1, r2, r3


def ranking_table(xPoints, yPoints, numbers, b, F, K, r1, r2, r3):
    fig, ax = plt.subplots()

    fig.patch.set_visible(False)
    ax.axis('off')
    ax.axis('tight')

    points = [numbers, xPoints, yPoints, b, F, K, r1, r2, r3]
    columns = ['i', 'F1', 'F2', 'b', 'F', 'K', 'R1', 'R2', 'R3']

    ax.table(cellText=np.array(points).T, colLabels=columns, loc='center')

    fig.tight_layout()

    plt.show()


