import numpy as np
import matplotlib.pyplot as plt
import random


def rejection_sampling(n, N):
    x = []
    y = []
    for i in range(0, N):
        while True:
            a = random.randrange(50)
            b = random.randrange(50)
            if (a - n) ** 2 + (b - n) ** 2 < n ** 2 and -a + b <= n and a + b >= 2 * n:
                x.append(a)
                y.append(b)
                break
    return x, y


def show(xPoints, yPoints, n, N):
    x = np.linspace(-10, 50)
    y = np.linspace(-10, 50)

    X, Y = np.meshgrid(x, y)

    F1 = (X - n) ** 2 + (Y - n) ** 2 - n ** 2
    F2 = -X + Y - n
    F3 = X + Y - 2 * n
    fig, ax = plt.subplots()

    line1 = ax.contour(X, Y, F1, [0], colors='red')
    line2 = ax.contour(X, Y, F2, [0], colors='green')
    line3 = ax.contour(X, Y, F3, [0], colors='blue')

    h1, _ = line1.legend_elements()
    h2, _ = line2.legend_elements()
    h3, _ = line3.legend_elements()

    ax.legend([h1[0], h2[0], h3[0]], ['(f_1 - n)^2 + (f_2 - n)^2 <= n^2', '-f_1 + f_2 <= n', 'f_1 + f_2 >= 2n'], loc='lower left')

    ax.set_aspect(1)


    annotations = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20']

    plt.scatter(xPoints, yPoints)

    plt.xlim(-5, 30)#размеры по х
    plt.ylim(-5, 30)# размеры по у

    for i, label in enumerate(annotations):
        plt.annotate(label, (xPoints[i], yPoints[i]))

    plt.grid(linestyle='--')
    plt.ylabel('F2 axis')
    plt.xlabel('F1 axis')
    plt.show()


def paint_cones(xPoints, yPoints, N, cones):
    fig, ax = plt.subplots()
    annotations = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18',
                   '19', '20']

    for i in range(0, N):
        if cones == 'inefficient':
            ax.vlines(xPoints[i], 0, yPoints[i])
            ax.hlines(yPoints[i], 0, xPoints[i])
        if cones == 'dominance':
            ax.vlines(xPoints[i], yPoints[i], 50)
            ax.hlines(yPoints[i], xPoints[i], 50)

    plt.xlim(-5, 30)
    plt.ylim(-5, 30)

    for i, label in enumerate(annotations):
        plt.annotate(label, (xPoints[i], yPoints[i]))

    plt.show()


def paint_ranking_graphic(xPoints, yPoints, K, n, N):
    x = np.linspace(-10, 50)
    y = np.linspace(-10, 50)

    X, Y = np.meshgrid(x, y)

    F1 = (X - n) ** 2 + (Y - n) ** 2 - n ** 2
    F2 = -X + Y - n
    F3 = X + Y - 2 * n
    fig, ax = plt.subplots()

    line1 = ax.contour(X, Y, F1, [0], colors='red')
    line2 = ax.contour(X, Y, F2, [0], colors='green')
    line3 = ax.contour(X, Y, F3, [0], colors='blue')

    ax.set_aspect(1)

    x_first_rank = []
    y_first_rank = []
    x_second_rank = []
    y_second_rank = []
    x_third_rank = []
    y_third_rank = []
    for i in range(0, N):
        if K[i] == 1:
            x_first_rank.append(xPoints[i])
            y_first_rank.append(yPoints[i])
        if K[i] == 2:
            x_second_rank.append(xPoints[i])
            y_second_rank.append(yPoints[i])
        if K[i] == 3:
            x_third_rank.append(xPoints[i])
            y_third_rank.append(yPoints[i])

    plt.xlim(-5, 30)
    plt.ylim(-5, 30)

    plt.plot(x_first_rank, y_first_rank, 'ro')
    plt.plot(x_second_rank, y_second_rank, 'g^')
    plt.plot(x_third_rank, y_third_rank, 'b*')
    plt.legend(['First klaster', 'Second klaster', 'Third klaster'], loc=2)

    plt.grid(linestyle='--')
    plt.ylabel('F2 axis')
    plt.xlabel('F1 axis')
    plt.show()
    